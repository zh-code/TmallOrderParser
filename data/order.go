package data

import "encoding/xml"

//Orders entity
type Orders struct {
	XMLName xml.Name      `xml:"Orders"`
	Text    string        `xml:",chardata"`
	Order   []OrderEntity `xml:"Order"`
}

//OrderEntity in Orders
type OrderEntity struct {
	Text            string `xml:",chardata"`
	CustomerDetails struct {
		Text    string `xml:",chardata"`
		Address struct {
			Text        string `xml:",chardata"`
			Name        string `xml:"Name"`
			FirstName   string `xml:"FirstName"`
			LastName    string `xml:"LastName"`
			AddressOne  string `xml:"AddressOne"`
			AddressTwo  string `xml:"AddressTwo"`
			District    string `xml:"District"`
			TownCity    string `xml:"TownCity"`
			CountyState string `xml:"CountyState"`
			Postcode    string `xml:"Postcode"`
			CountryCode string `xml:"CountryCode"`
		} `xml:"Address"`
		ContactDetails struct {
			Text      string `xml:",chardata"`
			Telephone string `xml:"Telephone"`
			Email     string `xml:"Email"`
		} `xml:"ContactDetails"`
	} `xml:"CustomerDetails"`
	ShippingDetails struct {
		Text           string `xml:",chardata"`
		ShippingMethod string `xml:"ShippingMethod"`
		Address        struct {
			Text        string `xml:",chardata"`
			Name        string `xml:"Name"`
			FirstName   string `xml:"FirstName"`
			LastName    string `xml:"LastName"`
			AddressOne  string `xml:"AddressOne"`
			AddressTwo  string `xml:"AddressTwo"`
			District    string `xml:"District"`
			TownCity    string `xml:"TownCity"`
			CountyState string `xml:"CountyState"`
			Postcode    string `xml:"Postcode"`
			CountryCode string `xml:"CountryCode"`
		} `xml:"Address"`
	} `xml:"ShippingDetails"`
	OrderLines struct {
		Text      string `xml:",chardata"`
		OrderLine struct {
			Text     string `xml:",chardata"`
			PID      string `xml:"PID"`
			VID      string `xml:"VID"`
			Title    string `xml:"Title"`
			Quantity string `xml:"Quantity"`
			VATRate  string `xml:"VATRate"`
			Price    string `xml:"Price"`
		} `xml:"OrderLine"`
	} `xml:"OrderLines"`
	AffiliateOrderID string `xml:"AffiliateOrderId"`
	Date             string `xml:"Date"`
	Value            string `xml:"Value"`
	ShippingCost     string `xml:"ShippingCost"`
	Currency         string `xml:"Currency"`
}
