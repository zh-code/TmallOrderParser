package main

import (
	"encoding/xml"
	"errors"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/tealeg/xlsx"
	"zh-code.com/TmalOrderParser/data"
)

func readFromExcel(name string) (*xlsx.File, error) {
	if !fileExists(name) {
		return nil, errors.New("File not found")
	}

	return xlsx.OpenFile(name)
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func saveToFile(name string, content []byte) {
	f, err := os.Create(name)
	if err != nil {
		fmt.Println("Failed to create new file with name " + name)
		return
	}
	defer f.Close()

	_, err = f.Write(content)
	if err != nil {
		fmt.Println("Failed to write content to file " + name)
	}
}

func processOrders(orderFile *xlsx.Sheet) []byte {
	orders := &data.Orders{}
	for i, row := range orderFile.Rows {
		if i != 0 {
			order := &data.OrderEntity{}
			if len(row.Cells) == 0 {
				break
			}

			order.CustomerDetails.Address.CountryCode = "CN"
			order.ShippingDetails.Address.CountryCode = "CN"
			order.OrderLines.OrderLine.VATRate = "0"
			order.Currency = "RMB"

			order.AffiliateOrderID = getStringWithLimit(row.Cells[0], 20)
			order.CustomerDetails.Address.Name = getStringWithLimit(row.Cells[1], 20)
			order.ShippingDetails.Address.Name = getStringWithLimit(row.Cells[14], 20)
			order.Value = row.Cells[10].String()
			if len(row.Cells[52].String()) > 0 {
				order.Value = row.Cells[52].String()
			}
			order.ShippingCost = row.Cells[6].String()

			address := removeJunkChar(row.Cells[15].String())

			if len(removeJunkChar(strings.Trim(row.Cells[41].String(), " "))) > 0 {
				address = removeJunkChar(row.Cells[41].String())
			}

			zip := address[len(address)-7:]

			addressSplit := strings.Split(address, " ")
			if len(addressSplit) == 4 {
				order.CustomerDetails.Address.CountyState = shrinkString(addressSplit[0], 20)
				order.ShippingDetails.Address.CountyState = shrinkString(addressSplit[0], 20)

				order.CustomerDetails.Address.TownCity = shrinkString(addressSplit[1], 20)
				order.ShippingDetails.Address.TownCity = shrinkString(addressSplit[1], 20)

				order.CustomerDetails.Address.District = shrinkString(addressSplit[2], 20)
				order.ShippingDetails.Address.District = shrinkString(addressSplit[2], 20)

				order.CustomerDetails.Address.AddressOne = shrinkString(strings.Replace(addressSplit[3], zip, "", 1), 20)
				order.ShippingDetails.Address.AddressOne = shrinkString(strings.Replace(addressSplit[3], zip, "", 1), 20)
			} else {
				fmt.Println("Too much addresses " + address)
			}
			zip = strings.Replace(zip, "(", "", 1)
			zip = strings.Replace(zip, ")", "", 1)
			order.CustomerDetails.Address.Postcode = zip
			order.ShippingDetails.Address.Postcode = zip

			phoneNumber := removeJunkChar(row.Cells[18].String())
			if len(phoneNumber) > 0 {
				order.CustomerDetails.ContactDetails.Telephone = shrinkString(phoneNumber, 20)
			} else {
				order.CustomerDetails.ContactDetails.Telephone = getStringWithLimit(row.Cells[17], 20)
			}

			orderDate := removeJunkChar(row.Cells[20].String())
			if len(orderDate) > 0 {
				orderSlice := strings.Split(orderDate, " ")
				if len(orderSlice) > 0 {
					order.Date = orderSlice[0]
				}
			}

			order.OrderLines.OrderLine.Title = removeJunkChar(row.Cells[21].String())
			order.OrderLines.OrderLine.Price = order.Value
			order.ShippingDetails.ShippingMethod = getStringWithLimit(row.Cells[24], 20)
			quantity := removeJunkChar(row.Cells[26].String())
			if len(quantity) > 0 {
				order.OrderLines.OrderLine.Quantity = quantity
			} else {
				order.OrderLines.OrderLine.Quantity = "0"
			}

			orders.Order = append(orders.Order, *order)
		}
	}
	xmlString, err := xml.MarshalIndent(orders, "", "    ")
	if err != nil {
		fmt.Println("Failed to parse Orders")
		return nil
	}
	return xmlString
}

func getStringWithLimit(cell *xlsx.Cell, limit int) string {
	if cell == nil {
		return ""
	}
	cellString := removeJunkChar(cell.String())
	if len(cellString) > limit {
		runes := []rune(cellString)
		return string(runes[0:limit])
	}
	return cellString
}

func shrinkString(str string, limit int) string {
	cellString := removeJunkChar(str)
	if len(cellString) > limit {
		runes := []rune(cellString)
		return string(runes[0:limit])
	}
	return cellString
}

func removeJunkChar(str string) string {
	if str[:1] == "'" {
		runes := []rune(str)
		str = string(runes[1:])
	}

	if str == "null" {
		return ""
	}
	return str
}

func main() {
	argsWithoutProg := os.Args[1:]
	if len(argsWithoutProg) > 2 || len(argsWithoutProg) < 2 {
		fmt.Println("Please enter filename and storeId")
	} else {
		xlsx, err := readFromExcel(argsWithoutProg[0])
		if err != nil {
			fmt.Println("Failed to open file: " + err.Error())
		} else {
			year, month, day := time.Now().Date()
			filename := fmt.Sprintf("Tmall_Orders_%s_%d%02d%02d_%02d%02d%02d.xml", argsWithoutProg[1], year, int(month), day, time.Now().Hour(), time.Now().Minute(), time.Now().Second())

			orders := xlsx.Sheets[0]
			orderBytes := processOrders(orders)
			saveToFile(filename, orderBytes)
			//returnOrder := xlsx.Sheets[1]
			//refundORder := xlsx.Sheets[2]
		}
	}
}
